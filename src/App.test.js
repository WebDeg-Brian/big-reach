import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

/**
 * Test to see if component renders.
 */

it("renders without crashing", () => {
  const element = document.createElement("div");
  ReactDOM.render(<App />, element);
  ReactDOM.unmountComponentAtNode(element);
});
