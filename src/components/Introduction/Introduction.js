import React from "react";

import Button from "../Button/Button";

import "./Introduction.scss";

import video from "../../static/videos/video.mov";
import chevron from "../../static/icons/introduction/chevron-down.svg";

/**
 * Logo component.
 */

const Introduction = () => {
  return (
    <>
      <section className="introduction">
        <video
          autoPlay
          muted
          loop
          src={video}
          className="introduction__video"
        />
        <div className="introduction__content container">
          <h1 className="introduction__title">Reach your audience</h1>
          <h2 className="introduction__subtitle">
            and launch into digital success...
          </h2>
          <Button link="/contact" content="Request a Quote" classes="button" />
          <Button link="/contact" content="Get in Touch" classes="button" />
        </div>
        <a href="#content" className="scroll">
          <img src={chevron} alt="Learn More" className="scroll__icon" />
        </a>
      </section>
    </>
  );
};

export default Introduction;
