import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./Button.scss";

/**
 * Button component.
 */

class Button extends Component {
  render() {
    if (this.props.external) {
      return (
        <a
          href={this.props.link}
          target="_blank"
          rel="noopener noreferrer"
          className={this.props.classes}
        >
          {this.props.content}
        </a>
      );
    } else {
      return (
        <Link to={this.props.link} className={this.props.classes}>
          {this.props.content}
        </Link>
      );
    }
  }
}

export default Button;
