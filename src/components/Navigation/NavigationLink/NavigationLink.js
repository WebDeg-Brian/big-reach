import React from "react";

import { Link } from "react-router-dom";

const NavigationLink = ({ title, link, onClick }) => {
  return (
    <li>
      <Link to={link} className="navigation__item" onClick={onClick}>
        {title}
      </Link>
    </li>
  );
};

export default NavigationLink;
