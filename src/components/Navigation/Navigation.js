import React from "react";

import NavigationLink from "../Navigation/NavigationLink/NavigationLink";
// import Dropdown from "../Navigation/Dropdown/Dropdown";

import "./Navigation.scss";

const Navigation = ({ open, onClick }) => {
  let classes = open ? "navigation navigation--open" : "navigation";

  return (
    <section className={classes}>
      <nav>
        <ul>
          <NavigationLink title="Home" link="/home" onClick={onClick} />
        </ul>
      </nav>
    </section>
  );
};

export default Navigation;
