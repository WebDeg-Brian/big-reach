import React from "react";

import "./NavigationToggle.scss";

const NavigationToggle = ({ open, onClick }) => {
  let classes = open
    ? "hamburger hamburger--spin is-active"
    : "hamburger hamburger--spin";

  return (
    <button className={classes} onClick={onClick} type="button">
      <span className="hamburger-box">
        <span className="hamburger-inner" />
      </span>
    </button>
  );
};

export default NavigationToggle;
