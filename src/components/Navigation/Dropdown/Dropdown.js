import React, { Component } from "react";

import NavigationLink from "../NavigationLink/NavigationLink";

import "./Dropdown.scss";

class Dropdown extends Component {
  state = {
    open: false
  };

  toggle = () => {
    this.setState({ open: !this.state.open });
  };

  generateLinks = () => {};

  render() {
    return (
      <li>
        <NavigationLink title="Digital Marketing" dropdown />
        <ul className="dropdown">
          <NavigationLink title="Test" link="/contact" />
        </ul>
      </li>
    );
  }
}

export default Dropdown;
