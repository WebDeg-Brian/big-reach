import React, { Component } from "react";

import Menu from "../Menu/Menu";

import Logo from "../Logo/Logo";

import "./Header.scss";

/**
 * Header component.
 */

class Header extends Component {
  state = {
    open: false,
    scrolled: false
  };

  /**
   * Toggle.
   *
   * Toggle the state of the menu.
   */

  toggle = () => {
    this.setState({ open: !this.state.open });
  };

  /**
   * Close.
   *
   * Close the menu.
   */

  close = () => {
    this.setState({ open: false });
  };

  /**
   * Handle Scroll.
   *
   * Append/remove a class to the header.
   */

  handleScrolll = () => {
    const scrolled = window.scrollY;

    if (scrolled) {
      this.setState({ scrolled: true });
    } else {
      this.setState({ scrolled: false });
    }
  };

  /**
   * Lifecycle methods.
   *
   * Bind and unbind events.
   */

  componentDidMount = () => {
    document.addEventListener("scroll", this.handleScrolll);
  };

  componentWillUnmount = () => {
    document.removeEventListener("scroll", this.handleScrolll);
  };

  render = () => {
    let classes = this.state.scrolled ? "header header--background" : "header";

    return (
      <>
        <header className={classes}>
          <div className="header__inner">
            <Logo onClick={this.close} />
            <Menu open={this.state.open} onClick={this.toggle} />
          </div>
        </header>
      </>
    );
  };
}

export default Header;
