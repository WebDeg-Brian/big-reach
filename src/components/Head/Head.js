import React, { Component } from "react";

import { Helmet } from "react-helmet";

/**
 * Head component.
 */

class Head extends Component {
  render() {
    return (
      <Helmet>
        <title>{this.props.title}</title>
        <meta name="description" content={this.props.description} />
      </Helmet>
    );
  }
}

export default Head;
