import React from "react";
import { Link } from "react-router-dom";

import "./Logo.scss";

import logo from "../../static/images/logo/logo.svg";

/**
 * Logo component.
 *
 * @param { object } onClick
 */

const Logo = ({ onClick }) => {
  return (
    <figure className="logo">
      <Link to="/" onClick={onClick}>
        <img src={logo} className="logo__image" alt="Big Reach Marketing" />
      </Link>
    </figure>
  );
};

export default Logo;
