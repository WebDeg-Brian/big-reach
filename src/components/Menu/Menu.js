import React from "react";

import Navigation from "../Navigation/Navigation";
import NavigationToggle from "../Navigation/NavigationToggle/NavigationToggle";
const Menu = ({ open, onClick }) => {
  return (
    <>
      <NavigationToggle open={open} onClick={onClick} />
      <Navigation open={open} onClick={onClick} />
    </>
  );
};

export default Menu;
