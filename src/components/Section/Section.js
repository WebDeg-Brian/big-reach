import React from "react";

import "./Section.scss";

/**
 * Section component.
 *
 * @param { object } props
 */

const Section = ({ title, subtitle }) => {
  return (
    <section className="section">
      <h2 className="section__title">{title}</h2>
      {subtitle ? <h4 className="section__subtitle">{subtitle}</h4> : ""}
    </section>
  );
};

export default Section;
