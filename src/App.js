import React from "react";
import { BrowserRouter } from "react-router-dom";

import "./stylesheets/main.scss";

import Header from "./components/Header/Header";
import Routes from "./routes/Routes";
import Footer from "./components/Footer/Footer";

/**
 * Application component.
 */

const App = () => {
  return (
    <div className="App">
      <BrowserRouter>
        <Header />
        <Routes />
        <Footer />
      </BrowserRouter>
    </div>
  );
};

export default App;
