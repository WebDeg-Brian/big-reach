import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";

import Home from "../pages/Home/Home";
import Contact from "../pages/Contact/Contact";
import _Error from "../pages/Error/Error";
class Routes extends Component {
  render() {
    return (
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/contact" exact component={Contact} />
        <Route component={_Error} />
      </Switch>
    );
  }
}

export default Routes;
