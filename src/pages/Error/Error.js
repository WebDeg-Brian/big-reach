import React from "react";

import Head from "../../components/Head/Head";

/**
 * Error page.
 */

const meta = {
  title: "Error | Big Reach Marketing",
  description: "Test description."
};

const Error = () => {
  return (
    <>
      <Head title={meta.title} description={meta.description} />
      <h1>Error</h1>
    </>
  );
};

export default Error;
