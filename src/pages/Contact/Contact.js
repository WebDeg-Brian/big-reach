import React from "react";

import Head from "../../components/Head/Head";

/**
 * Contact page.
 */

const meta = {
  title: "Contact | Big Reach Marketing",
  description: "Test description."
};

const Contact = () => {
  return (
    <>
      <Head title={meta.title} description={meta.description} />
      <h1>Contact</h1>
    </>
  );
};

export default Contact;
