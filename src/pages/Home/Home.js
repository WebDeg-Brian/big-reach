import React from "react";

import Head from "../../components/Head/Head";
import Introduction from "../../components/Introduction/Introduction";
import Section from "../../components/Section/Section";

/**
 * Home page.
 */

const meta = {
  title: "Home | Big Reach Marketing",
  description: "Test description."
};

const Home = () => {
  return (
    <>
      <Head title={meta.title} description={meta.description} />
      <Introduction />
      <Section
        title="Achieve your digital goals"
        subtitle="…clever campaigns with measurable results"
      >
        <div />
      </Section>
    </>
  );
};

export default Home;
