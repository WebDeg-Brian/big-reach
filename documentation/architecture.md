# Architecture

## Components

`src/components`

Components such as a logo, button etc.

## Pages

`src/pages`

Individual pages within which components will be rendered.

## Routes

`src/routes`

Router configuration and links.

## Static

`src/static`

Static assets such as images and videos.

## Stylesheets

`src/stylesheets`

Global styles (SASS).
